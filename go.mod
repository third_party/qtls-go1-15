module github.com/marten-seemann/qtls-go1-15

go 1.15

require (
	chainmaker.org/third_party/q-tls-common v1.0.0
	github.com/golang/mock v1.6.0
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a
)
